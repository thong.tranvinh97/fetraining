jQuery(document).ready(function($) {
    $('.fancybox').fancybox();

    $('.our-client-slider').slick({
        dots: false,
        infinite: false,
        arrows: false,
        slidesToShow: 2,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: 1200,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                dots: true,
                dotClass: 'slick-dots'
            }
        }]
    });

    // $(".navbar-toggle").click(function() {
    //     $(".navbar-collapse").collapse('toggle');
    // });
});